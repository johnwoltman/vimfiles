" John Woltman's vimrc
" based on Doug Black's vimrc article, http://dougblack.io/words/a-good-vimrc.html

" Initial Configs, affect other things {{{
let mapleader=","               " Change leader to comma, seems to be popular
" Start pathogen to autoload other plugins
execute pathogen#infect()
" }}}

" Syntax Highlighting {{{
" =======================
set t_Co=256                    " enable 256 colors
colorscheme mojave              " my sweet color scheme 
syntax enable                   " enable syntax highlighting
" }}}

" Tabs & Spaces {{{
" =================
set tabstop=4                   " visual spaces per tab, suits PEP8
set softtabstop=4               " number of spaces in tab when editing
set shiftwidth=4                " should be set to same as softtabstop
set expandtab                   " insert spaces instead of tabs
" }}}

" Improve the UI {{{
" ==================
set number                      " show line numbers
filetype plugin indent on       " load filetype-specifc plugins & indent files
set wildmenu                    " visual autocomplete for command menu
set lazyredraw                  " redraw only when necessary
set showmatch                   " highlight matches [{()}]
set laststatus=2                " always show airline status bar
set guifont=Meslo_LG_M_for_Powerline:h9:cANSI
"set guifont=Anonymous_Pro:h10.5:cANSI

" If we're using a GUI then increase the default window size
"if has("gui_running")
"    set columns=120 lines=35
"endif
" }}}

" Search {{{
" ==========
set incsearch                   " search as characters are entered
set hlsearch                    " highlight matches
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>
" }}}

" Folding {{{
" ===========
set foldenable                  " enable folding
set foldlevelstart=10           " open most folds by default
set foldnestmax=8               " maximum number of folds to nest
set foldmethod=syntax           " fold based on syntax
" space open/closes folds
nnoremap <space> za
" }}}

" Miscellaneous Keybindings {{{
" =============================
set bs=indent,eol,start         " make backspace act normally
" move vertically by *visual* line
nnoremap j gj
nnoremap k gk

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>
" save session
nnoremap <leader>s :mksession<CR>
" }}}

" CtrlP settings {{{
" ==================
let g:ctrlp_match_window = 'bottom,order:ttb'     " show top-to-bottom
let g:ctrl_switch_buffer = 0                      " new buffer when opening
let g:ctrlp_working_path_mode = 0                 " sync ctrlp's cwd with vim's
" the following can only be used if ag is insalled
" let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g  ""'
" }}}

" tmux support {{{
" ================
" allows cursor change in tmux mode
if exists('$TMUX')
    let &t_SI =  "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI =  "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI =  "\<Esc>]50;CursorShape=1\x7"
    let &t_EI =  "\<Esc>]50;CursorShape=0\x7"
endif
" }}}

" Window Size Restoration {{{
" =======================
" From http://vim.wikia.com/wiki/Restore_screen_size_and_position
if has("gui_running")
  function! ScreenFilename()
    if has('amiga')
      return "s:.vimsize"
    elseif has('win32')
      return $HOME.'\_vimsize'
    else
      return $HOME.'/.vimsize'
    endif
  endfunction

  function! ScreenRestore()
    " Restore window size (columns and lines) and position
    " from values stored in vimsize file.
    " Must set font first so columns and lines are based on font size.
    let f = ScreenFilename()
    if has("gui_running") && g:screen_size_restore_pos && filereadable(f)
      let vim_instance = (g:screen_size_by_vim_instance==1?(v:servername):'GVIM')
      for line in readfile(f)
        let sizepos = split(line)
        if len(sizepos) == 5 && sizepos[0] == vim_instance
          silent! execute "set columns=".sizepos[1]." lines=".sizepos[2]
          silent! execute "winpos ".sizepos[3]." ".sizepos[4]
          return
        endif
      endfor
    endif
  endfunction

  function! ScreenSave()
    " Save window size and position.
    if has("gui_running") && g:screen_size_restore_pos
      let vim_instance = (g:screen_size_by_vim_instance==1?(v:servername):'GVIM')
      let data = vim_instance . ' ' . &columns . ' ' . &lines . ' ' .
            \ (getwinposx()<0?0:getwinposx()) . ' ' .
            \ (getwinposy()<0?0:getwinposy())
      let f = ScreenFilename()
      if filereadable(f)
        let lines = readfile(f)
        call filter(lines, "v:val !~ '^" . vim_instance . "\\>'")
        call add(lines, data)
      else
        let lines = [data]
      endif
      call writefile(lines, f)
    endif
  endfunction

  if !exists('g:screen_size_restore_pos')
    let g:screen_size_restore_pos = 1
  endif
  if !exists('g:screen_size_by_vim_instance')
    let g:screen_size_by_vim_instance = 1
  endif
  autocmd VimEnter * if g:screen_size_restore_pos == 1 | call ScreenRestore() | endif
  autocmd VimLeavePre * if g:screen_size_restore_pos == 1 | call ScreenSave() | endif
endif
" }}}

" vim:foldmethod=marker:foldlevel=0

